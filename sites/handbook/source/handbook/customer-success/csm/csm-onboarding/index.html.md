---
layout: handbook-page-toc
title: "TAM Onboarding"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

View the [TAM Handbook homepage](/handbook/customer-success/csm/) for additional TAM-related handbook pages.

---

Onboarding for Technical Account Managers is a guided, methodical process to ensure new team members have the knowledge they need to be effective.

# PAGE UNDER DEVELOPMENT 
# MORE DETAILS TO BE ADDED IN FY23

#### Chorus Playlist

- [Chorus Playlist](https://chorus.ai/playlists/741604) gives you access to the recordings of the best TAM calls. The recordings are continuously updated, so keep this playlist bookmarked for best practices. 

## Meeting shadowing

To learn how we conduct both customer meetings and internal planning meetings, a new team member will shadow these meetings with one or more established members of the team.

[Meeting Shadowing](/handbook/customer-success/csm/csm-onboarding/shadowing/)

## TAM Office Hours

We host a TAM office hour weekly at 3:00PM and 11:00PM UTC to answer questions in a judgment-free space. You can add your questions to the agenda document linked in the Google Calendar invite and hop on the call to work through questions or learn how to find helpful resources.
