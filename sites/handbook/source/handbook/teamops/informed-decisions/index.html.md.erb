---
layout: handbook-page-toc
title: "Informed decisions — TeamOps"
canonical_path: "/handbook/teamops/informed-decisions/"
description: Informed decisions — TeamOps
twitter_image: "/images/opengraph/all-remote.jpg"
twitter_image_alt: "GitLab remote team graphic"
twitter_site: "@gitlab"
twitter_creator: "@gitlab"
---

## On this page
{:.no_toc}

- TOC
{:toc}

{::options parse_block_html="true" /}

<%= partial("handbook/teamops/_teamops_overview.erb") %>

# Teams must be informed by an objective, shared reality

The decisions teams make must be informed by an objective — and shared — reality. We ensure everyone can consume (self-serve) the same shared information. This starts with clear documentation in a single source of truth. Values manifest as behaviors which can be observed and quantified. And wherever possible, teams must default to transparency. While other management philosophies prioritize the speed of knowledge ***transfer***, TeamOps optimizes for the speed of knowledge ***retrieval***.

Action tenets and real-world examples are below. 

## Public by default

Conventional management philosophies may rely on intentional information silos or a "need-to-know-basis" model. This approach restricts transparency and optimizes for a reduction in misinformation. TeamOps flips this notion by requiring that information be [public by default](/handbook/values/#transparency) and optimizes for maximum contribution. 

This includes all information which is [explicitly not public](/handbook/communication/confidentiality-levels/#not-public), and it requires transparency across all functions of a business. The key question answered is no longer, "*How does the organization get information to the right people at the right time?*", but rather, "*How does the organization create a system where everyone can consume information and  contribute, regardless of level or function?*"

In practice, an organization which implements TeamOps would use a project management system that allows finance, engineering, communications, design, marketing, and sales (as example departments) to view data related to all other functions. Instead of requiring that one function requests permission for access to another function's data, the system is designed wall-less by default. This means the onus of management is *not* to build an information sharing or request framework. Instead, the onus is to *educate* team members on how to add information in a systematized manner and how to search for data within the system.

Scalable leadership is effective leadership. By writing guidance down transparently — in a way that others can correct, validate, or otherwise contribute — leadership scales beyond an individual or team, and even beyond company tenure. GitLab refers to this approach as [handbook-first](/company/culture/all-remote/handbook-first-documentation/). A core outcome is individuals are less dependent on others for direction and guidance. This is not because those inputs are not valued, but because they are inputted to a transparent system as opposed to a silo or limited group. 

**Here's an example:** [Livestreaming company meetings on a branded YouTube channel](https://youtu.be/XcqloQezOUg)

Shortly after GitLab Chief Revenue Officer Michael McBride joined the company in 2018, he [livestreamed a 1-to-1 meeting](https://youtu.be/XcqloQezOUg) with GitLab co-founder and CEO Sid Sijbrandij. As part of McBride's onboarding, Sid was asked to provide an impromptu pitch of GitLab. 

In a conventional organization, this interaction would likely be private and not recorded. By recording it and streaming it to the public on a branded YouTube channel, everyone is more informed — the two individuals on the call; GitLab team members past, present, and future; the wider community; customers and partners; candidates; etc. 

## Single Source of Truth (SSoT)

TeamOps deliberately structures data (policy, workflows, values, etc.) in a [single source of truth (SSoT)](/handbook/values/#single-source-of-truth). It is founded on the thesis that decisions are [better informed](/company/culture/all-remote/handbook-first-documentation/) when there is no such thing as a "latest version." There is only *the* version. 

Teams and functions may choose different mediums as the SSoT depending on the task and the nature of their work. TeamOps allows this type of SSoT flexibility, but requires that those who dictate the SSoT share that information transparently and [crosslink](/handbook/communication/#cross-link) where appropriate.

**Here's an example:** [Maintaining a single list of all applications used within the company](/handbook/business-technology/tech-stack/)

Maintaining a single source of truth for elements that few individuals need isn't difficult (e.g. using a single, persistent shared document for a [1-to-1 meeting](/handbook/leadership/1-1/) between a people manager and direct report). Honoring the single source of truth is harder for elements which are cross-functional by design, such as a list of all applications used across the entire organization. 

In conventional organizations, multiple departments may maintain their own version of this list. Crucially, this is either not acknowledged as a problem, or there is no known solution to removing duplication. In an organization powered by TeamOps, duplicate sources may exist initially. Once duplication is discovered, everyone can contribute to the solve (collaborating to create a SSoT) and one's default is to *actively work to remove duplication*. At GitLab, the application list [lives in our handbook](/handbook/business-technology/tech-stack/), and integrity is ensured thanks to [version control](/topics/version-control/). 

## Low-context communication

[Low-context communication](/company/culture/all-remote/effective-communication/) is marked by explicit *over* implicit, direct *over* indirect, simple *over* complex, and comprehensive *over* narrow. It is called low-context communications because individuals are not expected to have knowledge of others' history and background. In a work setting, this is common; most team members do not have preexisting relationships with their colleagues prior to joining the same team. By establishing a high bar for including as much context as is known in all information exchanges, decisions are enabled to be more informed. 

Each business function may have unique expectations on low-context communication (e.g. what classifies as low-context in sales may not in engineering). TeamOps requires that each function exhibit qualities of low-context communication which are reasonable for the demands of that function. If decisions within a function appear to be ill-informed, audit the expectations on context first. 

**Here's an example:** [Making a company-wide announcement that meaningfully changes a policy](/handbook/communication/#how-to-make-a-company-wide-announcement)

In an organization empowered by TeamOps, a department leader sends out a [company-wide message](/handbook/communication/#how-to-make-a-company-wide-announcement) to a Slack channel that includes every team member. Crucially, this message does not include *only* the news, but a link to a GitLab merge request *detailing what changed* ([diffs](https://docs.gitlab.com/ee/development/diffs.html)). 

The [merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/104440) which added the very copy you're reading now sheds light into the practice of low-context communication. The [DRI](/handbook/people-group/directly-responsible-individuals/) for the change also shares a link to the handbook and/or project page ("the news"), the merge request includes context on what's changing, and details on where to ask questions and contribute new iterations (including an optional [Ask Me Anything (AMA)](/handbook/communication/ask-me-anything/) session). Managers and team members have enough context to share feedback and apply these changes to their own teams in an informed way.

## Situational leadership

Informed decisions require the use of "It depends." when evaluating a response. In an organization powered by TeamOps, individuals and people managers alike are responsible for seeking information in order to respond in a scenario-appropriate way. 

Adopting a [situational leadership strategy](/blog/2021/11/19/situational-leadership-strategy/) brings an added layer of intelligence to the way leaders manage each individual, project, and decision. It acknowledges that leaders should adapt the way they communicate, provide guidance, and delegate work based on a list of weighed factors and considerations. This strategy enables more informed decisions, prevents a reliance on status quo, and presents new growth opportunities for team members.

When it comes to TeamOps, [situational leadership](https://situational.com/situational-leadership/) does not only apply to people managers. It also applies to individual contributors, as everyone is expected to be a [manager of one](/handbook/leadership/#managers-of-one). 

**Here's an example:** [Managing a bulletin to millions and an update to a learning module](https://youtu.be/Q5HPHZKecrQ?t=736)

[Situational leadership](/blog/2021/11/19/situational-leadership-strategy/) is exemplified when a leader adjusts behavior from scenario to scenario, rather than carrying emotions or biases from one scenario directly into the one they encounter next. In an organization powered by TeamOps, a leader who recognizes a material error in a planned bulletin for an audience of millions may pivot to risk-reduction mode and require that every minor update be vetted. Though this is inefficient and unenjoyable, the leadership fits the scenario. 

The significance of the leader's direct involvement began as "It depends," and became clearer as the variables were revealed. In that leader's next meeting, the topic shifts to revisions in an internal learning module with a person responsible for that program. The stakes are lower, the audience is smaller, and revisions are [two-way doors](/handbook/values/#make-two-way-door-decisions) (easily reversible). By seeking information on the audience, timeline, and impact, the leader is able to decouple the new decision from the prior decision, leading to greater delegation, fewer approval loops, and appropriate urgency. After all, "It depends."!

## Inclusivity 

Decisions are better informed overall when they include a maximally [diverse array of perspectives](/handbook/values/#seek-diverse-perspectives). In TeamOps, a [bias for asynchronous communication](/handbook/values/#bias-towards-asynchronous-communication) fosters [inclusion](/company/culture/inclusion/). By defaulting to written, asynchronous sharing, everyone contributes in the same size font. People of all backgrounds, abilities, and work styles are invited to participate in a way that serves their needs. The best idea wins, not the loudest voice in the meeting. 

TeamOps frees contributors from the conventional bounds of time zones and meetings. This generates more informed contributions from more parties, more thoughtful conversation, and more archived context for retrospectives and evaluations. 

**Here's an example:** [Changing a company operating principle](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/103753) 
   
A leader in GitLab's learning & development team hosted [Crucial Conversation](/handbook/leadership/crucial-conversations/) cohorts for six months. She noticed a common theme: people consistently struggled to say "no" at work. Rather than hosting a closed-door meeting to change an operating principle to address this, the leader proposed a change in a public forum ([a GitLab merge request](https://gitlab.com/gitlab-com/www-gitlab-com/-/merge_requests/103753)). 

Additionally, the leader posted a Slack message in the public `#values` channel asking for feedback and suggestions from anyone who felt compelled to contribute. Ultimately, [the DRI](/handbook/people-group/directly-responsible-individuals/) of the impacted handbook page — [GitLab Values](/handbook/values/) — came to a decision that was more informed, as it included a more diverse range of perspectives. The feedback is also well-documented for future reference and iterations. 

## Shared values

Informed decisions are best made in the context of a shared set of organizational values, which are one of the [prerequisites](/handbook/teamops/#prerequisites-for-TeamOps) for TeamOps. Without explicit values, there is no group identity or basis for group cohesiveness. Each organization may enlist unique values, but it's vital that team members within the organization understand and utilize them. The guard rails created by explicit, shared values provide more freedom for individual decision making. This leads to more informed decisions by removing guesswork on if and how values were applied during the decision making process.

**Here's an example:** [20+ ways GitLab values are integrated into decision making](/handbook/values/#how-do-we-reinforce-our-values)

GitLab [reinforces its values in over 20 different ways](/handbook/values/#how-do-we-reinforce-our-values), including what we select for during hiring, our default software settings, criteria for discretionary bonuses and promotions, and what we explicitly call out when making decisions. These intentional integrations increase the likelihood that decisions are informed by shared values. 

---

Return to the [TeamOps](/handbook/teamops/) homepage. 
