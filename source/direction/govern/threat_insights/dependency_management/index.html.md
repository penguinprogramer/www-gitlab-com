---
layout: secure_and_protect_direction
title: "Category Direction - Dependency Management"
---

- TOC
{:toc}

| | |
| --- | --- |
| Stage | [Govern](/direction/govern/) |
| Maturity | [Viable](/direction/maturity/) |
| Features & Demos | [Our Youtube playlist](https://www.youtube.com/playlist?list=PL05JrBw4t0Kp8oA6OJ6_wm7uw0muud_mZ) |
| Content Last Reviewed | `2022-09-01` |
| Content Last Updated | `2022-09-01` |

## Introduction and how you can help

Thanks for visiting this category strategy page on Vulnerability Management in GitLab. This category belongs to the [Threat Insights group](https://about.gitlab.com/handbook/product/categories/#threat-insights-group) of the Govern stage and is maintained by [Matt Wilson](https://about.gitlab.com/company/team/#matt_wilson) ([mwilson@gitlab.com](mailto:<mwilson@gitlab.com>)).

### Engage with us

At GitLab, we believe [everyone can contribute](https://about.gitlab.com/company/strategy/#contribute-with-gitlab). One of the simplest ways is by contributing your feedback! If you're a GitLab user or an interested security professional, we especially would love to hear from you. Check out all the ways you can engage with us and chose which one is right for you.

## Overview

This is a new category.  Content for this page is still being developed.